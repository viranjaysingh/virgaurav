﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DemoWebAppplicaton.Startup))]
namespace DemoWebAppplicaton
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
